By Wilson,Jabez 
Sudoku solver that uses the backtracking algorithm
explained here
https://codemyroad.wordpress.com/2014/05/01/solving-sudoku-by-backtracking/

-------------------------------------------------------------------------

To demonstrate the solver 
compile the front end as 
gcc frontEnd.c solver.c sudoku.c [-O -Wall -Werror -o awesomeName]

flags inside the [] are optional

execute as ./a.out or ./awesomeName

and follow the instructions on the console

--------------------------------------------------------------------------

sudoku.c is a file that can be used by itself as well,
for other solving algorithms or as a sudoku representer

all initialize functions are initialized with 81 character 
strings which represent each number of the grid row by row

example:

 --- --- --- 
|16.|.43|...|
|.98|.6.|..7|
|.75|...|2.4|
 --- --- --- 
|.2.|8..|9..|
|7.9|4.6|8.1|
|..3|..1|.5.|
 --- --- --- 
|2.4|...|13.|
|5..|.8.|69.|
|...|32.|.78|
 --- --- --- 

is represented as 
000320078500080690204000130003001050709406801020800900075000204098060007160043000

-------------------------------------------------------------------------

