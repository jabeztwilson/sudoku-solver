#include "sudoku.h"
#include "solver.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#define PUZZLE4 "000 320 078  500 080 690  204 000 130  003 001 050  709 406 801  020 800 900  075 000 204  098 060 007  160 043 000"

static void removeExtra(char something[]);

//####################################
//MAIN
//####################################
	int main(int argc, char const *argv[])
	{
		char solution[82];
		char puzzle[155];

		//intro messsage:
		printf("at the prompt enter the puzzle \nin the form of an 81 character string\nin rows starting from the bottom\nfor example if a grid is of the form\n");
		printf("789\n456\n123\nthe \'9\' character string would be 12345 6789(whitespaces are accepted)\n\n");
		printf("Example puzzle: %s\n",PUZZLE4);
		printf("More at http://sudoku.com.au/ \n");
		printf("Enter The puzzle\n>");

		//initializing puzzle
		fgets(puzzle,150,stdin);
		removeExtra(puzzle);
		assert(strlen(puzzle)==81);
		Sudoku s = initialize(puzzle);
		printf("Original Puzzle:\n");
		printSudoku(s);
		disposeSudoku(s);

		//getting solution
		Solve(puzzle,solution);

		//creating sudoku object ofr solution and printing it
		s = initialize(solution);
		printf("Solution Puzzle:\n");
		printSudoku(s);
		disposeSudoku(s);



		return EXIT_SUCCESS;
	}

	static void removeExtra(char something[]){
		int i,j;
		i=0;
		char c;
		while(something[i] != '\0'){

			if(something[i] == ' ' || something[i] == '\n'){
				j=i;
				while(something[j] != '\0'){
					c = something[j+1];
					something[j] = c;
					j++;
				}
				i--;
			}
			i++;
		}
	}