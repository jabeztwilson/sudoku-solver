#include "sudoku.h"
#include "solver.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#define PUZZLE1 "008100027050940001093006500000310854070000060345082000009700480600029070280003100"
#define PUZZLE3 "000000000000000000000000000000000000000000000000000000000000000000000000000000000"
//468135927752 9 486 
#define PUZZLE2 "000000000002048631193276548926317854871594263345682719539761480004829375287453190"
#define PUZZLE4 "000 320 078  500 080 690  204 000 130  003 001 050  709 406 801  020 800 900  075 000 204  098 060 007  160 043 000"
#define START 0
#define RUN 1
#define EXIT 2
#define EXCHANGE 3

//####################################
//Struct Definition
//####################################
	typedef struct _sudokuStack{
		Sudoku grid;
		int size;
	}sudokuStack;

	typedef struct _sudokuStack* SudokuStack;

//####################################
//Function Declaration
//####################################
	static SudokuStack newStack(char *initial);
	static void disposeStack(SudokuStack s);
	static void push(SudokuStack s,int num);
	static int pop(SudokuStack s);
	static int size(SudokuStack s);
	static int check(SudokuStack s,int num);
	static int topOriginal(SudokuStack s);
	static int top(SudokuStack s);
	static void transfer(SudokuStack s, char* sudokuString);
	static void removeExtra(char *something);

//####################################
//Function definitions
//####################################
	static void removeExtra(char something[]){
		int i,j;
		i=0;
		char c;
		while(something[i] != '\0'){
			if(something[i] == ' ' || something[i] == '\n'){
				j=i;
				while(something[j] != '\0'){
					c = something[j+1];
					something[j] = c;
					j++;
				}
				i--;
			}
			i++;
		}
	}
	
	static SudokuStack newStack(char *initial){
		SudokuStack ret = malloc(sizeof(struct _sudokuStack));
		ret->grid = initialize(initial);
		ret->size = 0;
		return ret;
	}
	static void disposeStack(SudokuStack s){
		disposeSudoku(s->grid);
		free(s);
	}
	static void push(SudokuStack s,int num){
		assert(s->size<82);
		if(num!=-1){
			insertElement(s->grid,num,s->size);
		}
		s->size++;
	}
	static int pop(SudokuStack s){
		assert(s->size>0);
		int ret = getElement(s->grid,s->size-1);
		if(getOriginal(s->grid,s->size-1)==0)
		removeElement(s->grid,s->size-1);
		s->size--;
		return ret;
	}

	static int size(SudokuStack s){
		return s->size;
	}

	static int top(SudokuStack s){
		return getElement(s->grid,s->size-1);
	}
	static int topOriginal(SudokuStack s){
		return getOriginal(s->grid,s->size-1);
	}

	static int check(SudokuStack s,int num){
		return isLegal(s->grid,num,s->size);
	}

	static void transfer(SudokuStack s, char* sudokuString){
		int i =0;
		while(i<81){
			sudokuString[i] = '0' + getElement(s->grid,i);
			i++;
		}
		sudokuString[i] = '\0';
	}

//####################################
//Solver
//####################################
int Solve(char* puzzle,char *solution){
	removeExtra(puzzle);
	SudokuStack s = newStack(puzzle);
	int oneTime = TRUE;
	int ret,next,ch;
	int state = START;
	while(ifSolved(s->grid) == FALSE && state != EXIT){
		//transfer(s,solution);printf("%s\n",solution);
		if(size(s) == 0){
			if(state == START){
				state = RUN;
			}
			else {
				printf("NOT FOUND\n");
				state = EXIT;
			}
		}


		next = 1;
		if(check(s,next)!=ORIGINAL){
			oneTime = TRUE;
			while(state == EXCHANGE || oneTime == TRUE){
				oneTime = FALSE;
				next = 1;
				if(state == EXCHANGE){
					state = RUN;
					next = pop(s);
					next++;
				}
				ch = check(s,next);
				while(next <= 9 && ch!=LEGAL){
					//printf("%d\n",ch);
					next++;
					ch = check(s,next);
				}
				if(next == 10){
					while((top(s) == 9 || topOriginal(s) !=0 ) && size(s)>0){
						pop(s);
					}
					if(size(s)!= 0){
						state = EXCHANGE;
					}
				}
				
				else{
					push(s,next);
				}
			}
		}
		else {
			push(s,-1);
		}

		if(size(s)==0){
			printf("NOT FOUND\n");
			state = EXIT;
		}
	}
	if(state == EXIT){
		ret = FALSE;
	}
	else {
		ret = TRUE;
		transfer(s,solution);
	}
	disposeStack(s);
	return ret;
}