/*
 * jabez Wilson 
 * 5027406
 *
 * sudoku.h stub
 *
 */

#define SUDOKU_SIZE 81

//returns by isLegal
 	#define LEGAL 1
	#define ROW_DUPLICATION 2
	#define COL_DUPLICATION 3
 	#define SYNTAX_ERROR 5
 	#define BOX_DUPLICATION 4
 	#define ORIGINAL 6

#define TRUE 1
#define FALSE 0

typedef struct _sudoku* Sudoku;

Sudoku initialize(char* puzzle);
void insertElement(Sudoku s,int number,int position);
int removeElement(Sudoku s,int position);
int isLegal(Sudoku s,int number,int position);
int ifSolved(Sudoku s);
void disposeSudoku(Sudoku s);
int getElement(Sudoku s,int position);
int getOriginal(Sudoku s,int position);
void printSudoku(Sudoku s);

