#include "sudoku.h"
#include "samuraiSolver.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define START 0
#define RUN 1
#define EXIT 2
#define EXCHANGE 3

#define NUM_GRIDS

#define NONE 0
#define GRID_1 1
#define GRID_2 10
#define GRID_3 100
#define GRID_4 1000 
#define GRID_5 10000

#define SAM_COL 15
#define SAM_ROW 15

#define MARK printf("MARKED\n");

//####################################
//Struct Definition
//####################################
	typedef struct _samuraiStack{
		Sudoku grid[NUM_GRIDS];
		int gridLocation[SAM_ROW*SAM_COL];
		int size;
	}samuraiStack;

	typedef struct _samuraiStack* SamuraiStack;

//####################################
//Function Declaration
//####################################
	static removeExtra(char *something);
	static SamuraiStack newStack(char *initial);
	static void disposeStack(SamuraiStack s);
	static void push(SamuraiStack s,int num);
	static int pop(SamuraiStack s);
	static int size(SamuraiStack s);
	static int check(SamuraiStack s,int num);
	static int topOriginal(SamuraiStack s);
	static int top(SamuraiStack s);
	static void transfer(SamuraiStack s, char* sudokuString);

//####################################
//MAIN
//####################################
	int main(int argc, char const *argv[])
	{
		char solution[82];
		int ret = Solve(PUZZLE1,solution);
		if(ret == TRUE)
		printf("Solution %s\n",solution);
		return EXIT_SUCCESS;
	}

//####################################
//Function definitions
//####################################
	static removeExtra(char *something){
		char *p = something;
		char *cursor = NULL;
		while(*p!='\0'){
			if(*p==' '|| *p == '\n'){
				cursor = p;
				while(*cursor!='\0'){
					*cursor = *(cursor+1);
					cursor++;
				}
			}
			p++;
		}
	}

	static SamuraiStack newStack(char *initial){
		SamuraiStack ret = malloc(sizeof(struct _SamuraiStack));
		char puzzle[NUM_GRIDS][82];

		int i;

		i=0;
		while(i<SAM_ROW*SAM_COL){
			ret->gridLocation[i]=0;
		}

		
		ret->grid = initialize(initial);
		ret->size = 0;
		return ret;
	}
	static void disposeStack(SamuraiStack s){
		i=0;
		while(i<5){
			disposeSudoku(s->grid[i]);
		}
		free(s);
	}

	static void push(SamuraiStack s,int num){
		assert(s->size<226);
		if(num!=-1)
		insertElement(s->grid,num,s->size);
		s->size++;
	}
	static int pop(SamuraiStack s){
		assert(s->size>0);
		int ret = getElement(s->grid,s->size-1);
		if(getOriginal(s->grid,s->size-1)==0)
		removeElement(s->grid,s->size-1);
		s->size--;
		return ret;
	}

	static int size(SamuraiStack s){
		return s->size;
	}

	static int top(SamuraiStack s){
		return getElement(s->grid,s->size-1);
	}
	static int topOriginal(SamuraiStack s){
		return getOriginal(s->grid,s->size-1);
	}

	static int check(SamuraiStack s,int num){
		return isLegal(s->grid,num,s->size);
	}

	static void transfer(SamuraiStack s, char* sudokuString){
		int i =0;
		while(i<81){
			sudokuString[i] = '0' + getElement(s->grid,i);
			i++;
		}
		sudokuString[i] = '\0';
	}

//####################################
//Solver
//####################################
int Solve(char* puzzle,char *solution){
	SudokuStack s = newStack(puzzle);
	int ret,next,ch;
	int state = START;
	while(ifSolved(s->grid) == FALSE && state != EXIT){
		//transfer(s,solution);printf("%s\n",solution);
		if(size(s) == 0){
			if(state == START){
				state = RUN;
			}
			else {
				printf("NOT FOUND\n");
				state = EXIT;
			}
		}


		next = 1;
		if(check(s,next)!=ORIGINAL){
			next = 1;
			if(state == EXCHANGE){
				state = RUN;
				next = pop(s);
				next++;
			}
			ch = check(s,next);
			while(next <= 9 && ch!=LEGAL){
				//printf("%d\n",ch);
				next++;
				ch = check(s,next);
			}
			if(next == 10){
				while((top(s) == 9 || topOriginal(s) !=0 ) && size(s)>0){
					pop(s);
				}
				if(size(s)!= 0){
					state = EXCHANGE;
				}
			}
			else{
				push(s,next);
			}
		}
		else {
			push(s,-1);
		}
		if(size(s)==0){
			printf("NOT FOUND\n");
			state = EXIT;
		}
	}
	if(state == EXIT){
		ret = FALSE;
	}
	else {
		ret = TRUE;
		transfer(s,solution);
	}
	disposeStack(s);
	return ret;
}

