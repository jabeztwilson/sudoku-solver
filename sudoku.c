/*
 * Jabez Wilson
 * 50274006
 *
 * Sudoku solver using the backtracking algorithm
 *
 * Version 1.1
 */

#include "sudoku.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

//#define MAIN_TEST

#define VALUE_FOUND 1
#define VALUE_NOTFOUND 0

#define GRID_SIDE 9
#define BOX_SIZE 9
//DEBUG
 #define MARK printf("MARKED\n");

//test stuff
 #define PUZZLE1 "008100027050940001093006500000310854070000060345082000009700480600029070280003100"
 #define PUZZLE2 "111111111111111111111111111111111111111111111111111111111111111111111111111111111"
 #define PUZZLE3 "000000000000000000000000000000000000000000000000000000000000000000000000000000000"

//####################################
//Struct Definition
//####################################

	struct _sudoku{
		int solution[SUDOKU_SIZE];
		int puzzle[SUDOKU_SIZE];
		int unsolvedSlots;
	};


//####################################
//Static Functions 
//####################################
	#ifdef MAIN_TEST
		static void testInitialize();
		static void testIsLegal();
		static void testGetElement();
		static void testGetOriginal();
		static void testInsertElement();
		static void testRemoveElement();
		static void testIsSolved();
	#endif

//####################################
//Main
//####################################
	#ifdef MAIN_TEST
		int main(int argc, char const *argv[])
		{
			testInitialize();

			testIsLegal();
			testGetElement();
			testGetOriginal();
			testInsertElement();
			testRemoveElement();
			testIsSolved();

			printf("PASSED ALL\n");
			return EXIT_SUCCESS;
		}
	#endif

//####################################
//Function definition
//####################################
	Sudoku initialize(char* puzzle){
		assert(strlen(puzzle) == SUDOKU_SIZE);
		int i =0;

		Sudoku ret = malloc(sizeof(struct _sudoku)*SUDOKU_SIZE);
		ret->unsolvedSlots =0;
		i=0;
		while(i<SUDOKU_SIZE){
			assert(puzzle[i]<='9' && puzzle[i]>='0');
			ret->solution[i] = puzzle[i] - '0';
			ret->puzzle[i] = puzzle[i] - '0';
			if(ret->puzzle[i] ==0){
				ret->unsolvedSlots++;
			}
			i++;
		}
		return ret;
	}

	void disposeSudoku(Sudoku s){
		free(s);
	}

	void insertElement(Sudoku s,int number,int position){
		assert(isLegal(s,number,position)==LEGAL);
		s->solution[position] = number;
		s->unsolvedSlots--;
	}
	
	int removeElement(Sudoku s,int position){
		assert(position>=0 && position <81);
		assert(s->puzzle[position]==0);
		int ret = s->solution[position];
		s->solution[position] = 0;
		s->unsolvedSlots++;
		return ret;
	}

	int isLegal(Sudoku s,int number,int position){
		int ret = LEGAL;
		int status = VALUE_NOTFOUND;
		int i,j,k,l;
		if(number <1 || number >9 || position <0 || position >80){
			ret = SYNTAX_ERROR;
			status = VALUE_FOUND;
		}

		//if occupied by oriignal
			if(s->puzzle[position] != 0 && status == VALUE_NOTFOUND){
				ret = ORIGINAL;
				status = VALUE_FOUND;
			}
		
		int x = position%GRID_SIDE,y = position/GRID_SIDE;

		//checking the row of position;
			i=0;j=y;
			while(i<GRID_SIDE && status == VALUE_NOTFOUND){
				if(s->solution[j*GRID_SIDE + i] == number){
					//MARK printf("<%d %d>\n",j*GRID_SIDE + i,s->solution[j*GRID_SIDE + i]);
					ret = ROW_DUPLICATION;
					status = VALUE_FOUND;
				}
				i++;
			}

		//checking the col of position;
			i=x;j=0;
			while(j<GRID_SIDE && status == VALUE_NOTFOUND){
				if(s->solution[j*GRID_SIDE + i] == number){
					ret = COL_DUPLICATION;
					status = VALUE_FOUND;
				}
				j++;
			}

		//checking the box
			i = x/3;j=y/3;
			i = i*3;
			j= j*3;
			k = 0;	
			while(k<BOX_SIZE/3 && status == VALUE_NOTFOUND){
				l=0;
				i=x/3;
				i = i*3;
				while(l<BOX_SIZE/3 && status == VALUE_NOTFOUND){
					if(s->solution[j*GRID_SIDE + i] == number){
						ret = BOX_DUPLICATION;
						status = VALUE_FOUND;
					}
					l++;
					i++;
				}
				k++;
				j++;
			}
			return ret;
	}

	int ifSolved(Sudoku s){
		int ret = FALSE;
		if(s->unsolvedSlots ==0){
			ret = TRUE;
		}
		return ret;
	}

	int getElement(Sudoku s,int position){
		return s->solution[position];
	}

	int getOriginal(Sudoku s,int position){
		return s->puzzle[position];
	}

	void printSudoku(Sudoku s){
		int i,j;
		i = GRID_SIDE -1;
		j = 0;
		int tmp;
		printf(" --- --- --- \n");
		while(i>=0){
			j=0;
			printf("|");
			while(j<GRID_SIDE){
				tmp = getElement(s,i*GRID_SIDE + j);
				if(tmp == 0)printf(".");
				else printf("%d",tmp );
				j++;
				if(j%3==0)printf("|");
			}
			if(i%3==0)printf("\n --- --- --- ");
			i--;
			printf("\n");
		}

	}

//####################################
//Tests 
//####################################
	#ifdef MAIN_TEST
		static void testInitialize(){
			Sudoku s = initialize(PUZZLE2);
			int i=0;
			while(i<81){
				assert(s->solution[i]==1);
				assert(s->puzzle[i]==1);
				i++;
			}
			assert(s->unsolvedSlots == 0);
			disposeSudoku(s);

			s = initialize(PUZZLE3);
			i=0;
			while(i<81){
				assert(s->solution[i]==0);
				assert(s->puzzle[i]==0);
				i++;
			}
			assert(s->unsolvedSlots == 81);
			disposeSudoku(s);
		}

		static void testIsLegal(){
			Sudoku s = initialize(PUZZLE1);
			assert(isLegal(s,7,0)==ROW_DUPLICATION);
			assert(isLegal(s,6,0)==COL_DUPLICATION);
			assert(isLegal(s,9,0)==BOX_DUPLICATION);
			assert(isLegal(s,4,0)==LEGAL);
			assert(isLegal(s,-1,0)==SYNTAX_ERROR);
			assert(isLegal(s,7,444)==SYNTAX_ERROR);
			assert(isLegal(s,7,2)==ORIGINAL);
			disposeSudoku(s);
		}

		static void testGetElement(){
			Sudoku s = initialize(PUZZLE2);
			int i =0;
			while(i<81){
				assert(getElement(s,i)==1);
				i++;
			}
			disposeSudoku(s);
		}

		static void testGetOriginal(){
			Sudoku s = initialize(PUZZLE3);
			int i =0;
			while(i<81){
				assert(getElement(s,i)==0);
				assert(getOriginal(s,i)==0);
				i++;
			}
			insertElement(s,2,0);
			assert(getOriginal(s,0)==0);
			assert(getElement(s,0)==2);
			disposeSudoku(s);
		}

		static void testInsertElement(){
			Sudoku s = initialize(PUZZLE3);
			
			assert(getElement(s,0)==0);
			insertElement(s,9,0);
			assert(getElement(s,0)==9);

			disposeSudoku(s);
		}

		static void testRemoveElement(){
			Sudoku s = initialize(PUZZLE2);
			assert(getElement(s,0)!=0);
			removeElement(s,0);
			assert(getElement(s,0)==0);
			disposeSudoku(s);
		}

		static void testIsSolved(){
			Sudoku s = initialize(PUZZLE2);
			assert(ifSolved(s)==TRUE);
			disposeSudoku(s);
			s = initialize(PUZZLE3);
			assert(ifSolved(s)==FALSE);
			disposeSudoku(s);
		}
	#endif